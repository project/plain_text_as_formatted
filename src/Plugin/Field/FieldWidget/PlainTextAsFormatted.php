<?php

namespace Drupal\text\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'text_textfield' widget.
 *
 * @FieldWidget(
 *   id = "plain_text_as_formatted",
 *   label = @Translation("Plain text as formatted"),
 *   field_types = {
 *     "string",
 *   },
 * )
 */
class PlainTextAsFormatted extends TextfieldWidget {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $main_widget = parent::formElement($items, $delta, $element, $form, $form_state);

    $element = $main_widget['value'];
    $element['#type'] = 'text_format';
    $element['#format'] = isset($items[$delta]->format) ? $items[$delta]->format : NULL;
    $element['#base_type'] = $main_widget['value']['#type'];
    return $element;
  }

}
