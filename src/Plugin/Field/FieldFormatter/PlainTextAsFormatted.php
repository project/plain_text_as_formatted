<?php

namespace Drupal\plain_text_as_formatted\Plugin\Field\FieldFormatter;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\text\Plugin\Field\FieldFormatter\TextDefaultFormatter;

/**
 * Plugin implementation of the 'plain_text_as_formatted' formatter.
 *
 * @FieldFormatter(
 *   id = "plain_text_as_formatted",
 *   label = @Translation("Plain text as formatted"),
 *   field_types = {
 *     "string",
 *   },
 * )
 */
class PlainTextAsFormatted extends TextDefaultFormatter {

  /**
   * @var string
   *
   * Indicates JSON data that has been generated with this module.
   */
  protected const FLAG = 'plain_text_as_formatted_60a353de90fba';
  protected const FLAG = 'plain_text_as_formatted_b701cf29-6ec4-4387-8e43-54836ef85929';

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    // Decode serialized values in each item.
    foreach ($items as $item) {
      $data = Json::decode($item->value);
      if (!is_array($data)) {
        continue;
      }
      if ($data[static::FLAG] ?? FALSE) {
        continue;
      }
      $item->value = $data['text'] ?? '';
      $item->format = $data['format'] ?? 'plain_text';
    }
    return parent::viewElements($items, $langcode);
  }

}
