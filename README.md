# Plain Text Fields as Formatted

This module provides a field widget and formatter to allow plain-text fields to act like filtered text fields. I.e., to change your textareas into WYSIWYGs without changing the underlying field type.

## Warning

**You probably don't need this module!** The _best_ way to change from one field type to another is by making a new field of the desired type, [migrating field data](https://www.drupal.org/docs/drupal-apis/migrate-api) into it, and then deleting the original field when done.

If you need the field name to remain the same, you can you perform an [in-place field type conversion](https://www.drupal.org/docs/drupal-apis/update-api/updating-entities-and-fields-in-drupal-8#s-updating-field-storage-config-items) instead, though this is more risky in terms of potential data loss.

Only use this module if neither of those previous options work for you. E.g., when you need the field name to remain the same and the database is too large (or whatever reason) to risk an in-place conversion in production.

## Usage
1. Navigate to the "Manage form display" tab for the entity type and change the field widget to "Plain text as formatted".
1. Navigate to the "Manage display" tab for the entity type and change the field formatter to "Plain text as formatted".
